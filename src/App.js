import './App.css';

// UserContext
import {UserProvider} from './UserContext'


import {Fragment, useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';

// Web Components
import AppNavBar from './AppNavBar';

// Web Pages
import Home from './pages/Home.js';
import Courses from './pages/Courses'
import CourseView from './components/CourseView'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'

/*
  JSX are similar to HTML tags, with one major difference that is being able to apply Javascript code. 
*/

/*
  react-router-dom - allows us to simulate changing pages in react. By default, react is used for SPA
    SPA - Single Page Application

  Router (BrowserRouter) - used to wrap components that uses react-router-dom and allows the use of routes and the routing system. not wrapping the Routes and Route components will render an error since the dev tries to use them without the BrowserRouter dependency

  Routes - holds all the Route components (it is used to wrap Route elements that are meant to hold the endpoint and the component to be rendered in the page)

  Route - assigns an endpoint and displays the appropriate page component for that endpoint
    path - assigns the endpoint through string data type
    element attribute - assigns the page component to be rendered/displayed at that particular endpoint
*/

function App() {
  
  const [user, setUser] = useState ({ 
    //email: localStorage.getItem('email')
     email: null,
     isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(()=>{
      console.log(user);
      console.log(localStorage);
    }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
          <Container fluid>
            <Routes>
              <Route path ="/" element= {<Home />} />
              <Route path ="/courses" element= {<Courses />} />
              <Route path ="/courses/:courseId" element= {<CourseView />} />
              <Route path ="/register" element= {<Register />} />
              <Route path ="/login" element= {<Login />} />
              <Route path ="/logout" element= {<Logout />} />
              <Route path='*' element={<Error />} />
            </Routes>
          </Container>
        </Router>         
    </UserProvider>
  );
}


// function App() {
//   return (
//     <Fragment>
//       <AppNavBar />
//          <Container> 
//           {<Home />
//           <Courses  />}
//           {/*<Register />*/}
//           <Login />
//         </Container>
//     </Fragment>
//   );
// }

export default App;
