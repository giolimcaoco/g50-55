// react permits the use of object literals if the dev will import multiple components from the same dependency
import {Button, Row, Col} from 'react-bootstrap';
import {Fragment} from 'react';
import {Navigate} from 'react-router-dom';
import { Link } from 'react-router-dom';



// export default function Banner(error){
// 	return (
// 			<Row>
// 				<Col className="p-5">
// 					{
// 						(error.name !== 'NotFound') ?
// 							<Fragment>
// 								<h1>Zuitt Coding Bootcamp</h1>
// 								<p>Opportunities for everyone, everywhere</p>
// 								<Button variant="primary">Enroll Now</Button>
// 							</Fragment>
// 							:
// 							<Fragment>
// 								<h1>Page Not Found</h1>
// 								<p>Go back to the <a href="/">homepage</a></p>
// 							</Fragment>
// 					}
// 				</Col>
// 			</Row>
// 		)
// }


export default function Banner({data}){
	console.log(data)
	const {title, content, destination, label} = data;
	return(
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-primary" to={destination}>{label}</Link>
			</Col>
		</Row>
	)
}


