import {Button, Row, Col, Card} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'



/*

----------Activity S50


export default function CourseCard(){
	return (
			//<Row className = "mt-3 mb-3">
				//<Col xs={12}>
					<Card className = "cardHighlight mb-1">
						<Card.Body>
							<Card.Title>
								<h5>Sample Course</h5>
							</Card.Title>
							<Card.Text><strong>Description:</strong><p>This is a sample course offering</p></Card.Text>
							<Card.Text><strong>Price:</strong><p>PhP 40,000</p></Card.Text>
							<Button variant="primary">Enroll</Button>
						</Card.Body>
					</Card>
				//</Col>
			//</Row>
	)
}*/

// we can try to deconstructure the props object received from the parent component (Courses.js) so that the main object from the mock database will be accessed. 

export default function CourseCard({courseProp}) {

    console.log(courseProp);    
    console.log(typeof courseProp);    

    // destructure the courseProp object for more code readability and ease of coding for the part of the dev (doesnt have to access multiple objects before accessing the needed properties)

    const { name, description, price, _id } = courseProp

    /*
		use the state hook for this component to be able to store its state
		States are used to keep track of the information related to individual components

			SYNTAX

				const [ getter, setter ] = useState (initialGetterValue)
    */

     //const [ count, setCount ] = useState (0);
    // returns an array with the first element being a value and the second element as a function thats used to change the value of the first element

	/*
		function enroll() keeps track of the enrollees for a course

		the setter function for useState are asynchronous allowing it to be executed separately from other codes (setCount is executed while the console.log is already executed)
	*/

	// const [ seats, setSeats ] = useState (10)
	// const [ isOpen, setIsOpen ] = useState(true)


/* ----------Activity 51

    function enroll(){
    	setCount (count+1)
    	setSeat (seat-1)
    	console.log(`Enrollees: ${count}`)

    	if (seat === 0) {
    		alert `No more seats available`
    	} else {
    		console.log(`Seats Available: ${seat}`)
    	}
    }*/

    // function enroll(){
       
    //         setCount(count + 1);
    //         console.log(`Enrollees: ${count}`);
    //         setSeats(seats - 1);
    //         console.log(`Seats: ${seats}`);
    //     } 

/*
	effect hook - allows us to execute a piece of code whenever a component gets rendered or if a value of the state changes

	useEffect - requires two arguments: function and a dependency array where BOTH ARE NEEDED/AMUST
		function - to set which lines of codes are to be executed
		dependency array - identifies which variable/s are to be listened to, in terms of changing state for the function to be executed
		if the array is left empty, the codes will not listen to any variable but the codes will be executed once.
*/

	// useEffect(() => {
	// 	if (seats === 0) {
	// 		setIsOpen (false)
	// 	}
	// }, [seats]);

/*
	two-way binding
		refers to the domino effect of changing the state going into the change of the display/UI in the frontend
*/	

    return (
        <Card className = "cardHighlight mb-4 pb-2">
            <Card.Body>
                <Card.Title>{name}</Card.Title>

                {/*<Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>*/}
{/*
                <Card.Text>Enrollees: {count}</Card.Text>*/}

                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

            </Card.Body>
        </Card>
    )
}


 //    { isOpen ? 
 //    <Button variant="primary" onClick= {enroll} /*disabled={seats===0}*/>Enroll</Button>
 //    :
 //    <Button variant="primary" onClick= {enroll} disabled>Enroll</Button>
	// }