// mock DB
const coursesData = [
	{
		id:"wdc001",
		name: "PHP-Laravel",
		description: "Nunc semper quam quis felis semper, nec consequat sem accumsan. Nunc sit amet molestie ligula, id porttitor tellus. Maecenas facilisis ultrices turpis, sit amet semper magna porttitor eu..",
		price: 45000,
		onOffer: true
	},
	{
		id:"wdc002",
		name: "Phyton-Django",
		description: "Aenean at felis molestie, rutrum magna sit amet, eleifend lorem. Donec at luctus nunc, vitae tristique massa. Mauris finibus in nibh nec iaculis. Phasellus condimentum leo vitae aliquet laoreet. ",
		price: 50000,
		onOffer: true
	},
	{
		id:"wdc003",
		name: "JAVA-Springboot",
		description: "Morbi quis magna odio. Nullam placerat venenatis leo. Morbi sollicitudin tempor aliquam. Nunc consequat suscipit porttitor.",
		price: 40000,
		onOffer: true
	}
];

export default coursesData;
