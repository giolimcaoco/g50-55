import React from 'react';

// create UserContext object
/*
	createContext method allows us to store a different data inside an object, in this case called UserContext. This set of information ca be shared to other components within the app
*/
const UserContext = React.createContext(); 


/*
	Provider component allows the other components to consume/use the context object and supply the necessary information needed to the context object
*/

// export without default needs destructuring before being able to be imported by other components
export const UserProvider = UserContext.Provider; 

export default UserContext