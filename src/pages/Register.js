import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

// UserContext
import UserContext from '../UserContext'

export default function Register(){

		const { user, setUser } = useContext(UserContext);

		const navigate = useNavigate()
		
		// state hooks to store the values of the input fields
		const [ email, setEmail ] = useState ('');
		const [ firstName, setFirstName ] = useState ('');
		const [ lastName, setLastName ] = useState ('');
		const [ mobileNo, setMobileNo ] = useState ('');
		const [ password1, setPassword1 ] = useState ('');
		const [ password2, setPassword2 ] = useState ('');
		// state hooks to determine whether the submit button is enabled or not
		const [ isActive, setIsActive ] = useState (false);


		// to check if we have successfully implemented the 2-way binding
		/*
			whenever a state of a component changes, the component renders the whole component executing the code found in the component itself
			e.target.value allows us to gain access the input fields current value to be used when submitting form data
		*/
		console.log (email);
		console.log (firstName);
		console.log (lastName);
		console.log (mobileNo);
		console.log (password1);
		console.log (password2);

		useEffect(() => {
			if ((email !== '' && firstName !== '' && lastName !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '' ) && (password1 === password2)) {
				setIsActive (true)
			} else {
				setIsActive (false)
			}
		},[email, firstName, lastName, mobileNo, password1, password2]);
		

		function registerUser(e){
			e.preventDefault();

			//localStorage.setItem('email', email)

			// setUser ({
			// 	email: localStorage.getItem('email')
			// })

			// // clear input fields
			// setEmail('');
			// setPassword1('');
			// setPassword2('');

			// alert('Thank you for registering!')


			fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify ({
					email: email
				})
			})
			.then (res => res.json())
			.then (res =>  {

				if (res === true){
					Swal.fire ({
						title: 'Email already used',
						icon: 'error',
						text: 'Please login instead, or register with new email address.'
					})
					console.log("Duplicate Email")
				} else {

					fetch (`${process.env.REACT_APP_API_URL}/users/register`, {
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							email: email,
							firstName: firstName, 
							lastName: lastName,
							mobileNo: mobileNo,
							password: password1
							
						})
					})
					.then (res => res.json())
					.then (res => {
						if (res === true) {
							Swal.fire({
								title: "Successfully registered",
								icon: "success",
								text: "Thank you for registering, you may now login."
							})
							navigate('/login')
						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again and fill out the form."
							})
						}
					})
				}
			})

			setFirstName('');
			setLastName('');
			setMobileNo('');
			setEmail('');
			setPassword1('');
			setPassword2('');

			console.log('Registration successful')

		}

	return (
		(user.id !== null) ?
			<Navigate to='/login' />
		:
		<Container>
			<h1>Register an account</h1>
	    	<Form onSubmit = {(e)=>registerUser(e)}>
	      		<Form.Group controlId="userEmail">
	       			<Form.Label>Email address</Form.Label>
	        			<Form.Control 
	        				type="email" 
	        				placeholder="Enter email" 
	        				value={email}
	        				onChange={e => setEmail(e.target.value)}
	        				required/>
	        			<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
	     		</Form.Group>

	     		<Form.Group controlId="firstName">
	     		    <Form.Label>First Name</Form.Label>
	     			    <Form.Control 
	     		    		type="firstName" 
	     		    		placeholder="First Name" 
	     		    		value={firstName}
	     		    		onChange={e => setFirstName(e.target.value)}
	     		    		required/>
	     		</Form.Group>

	     		<Form.Group controlId="lastName">
	     		    <Form.Label>Last Name</Form.Label>
	     			    <Form.Control 
	     		    		type="lastName" 
	     		    		placeholder="Last Name" 
	     		    		value={lastName}
	     		    		onChange={e => setLastName(e.target.value)}
	     		    		required/>
	     		</Form.Group>

	     		<Form.Group controlId="mobileNo">
	     		    <Form.Label>Mobile Number</Form.Label>
	     			    <Form.Control 
	     		    		type="mobileNo" 
	     		    		placeholder="Enter 11 digits mobile number" 
	     		    		value={mobileNo}
	     		    		onChange={e => setMobileNo(e.target.value)}
	     		    		required/>
	     		</Form.Group>

	     		<Form.Group controlId="password1">
			        <Form.Label>Password</Form.Label>
			    	    <Form.Control 
			        		type="password" 
			        		placeholder="Password" 
			        		value={password1}
			        		onChange={e => setPassword1(e.target.value)}
			        		required/>
			    </Form.Group>

			    <Form.Group controlId="password2">
			        <Form.Label>Verify Password</Form.Label>
			    		<Form.Control 
			        		type="password" 
			        		placeholder="Password" 
			        		value={password2}
			        		onChange={e => setPassword2(e.target.value)}
			        		required/>
			    </Form.Group>
	    		{ isActive ? 
				
	      			<Button variant="primary" type="submit" id="submitBtn" className="mb-3">Submit</Button>
	  	  		
	      			:
				
	      			<Button variant="primary" type="submit" id="submitBtn" className="mb-3" disabled>Submit</Button>
	  	  	
	  	  		}
		    </Form>
	    </Container>
	  );

} 


/* 	-------------------------Activity S55

	Activity:
	1. Refactor the Register page to include additional form inputs for the user's first name, last name and mobile number.
	2. Refactor the useEffect hook for the form validation to include the user’s first name, last name and mobile number with at least 11 digits.
	3. Make the register feature work that will prevent the user from registering if the email already exists.
	4. Redirect the user to the Login page upon successful registration.
	5. Push your updates to S50-55 repo.
	6. Add another the remote link and push to git with the commit message of Add activity code - S55.
	7. Add the link in Boodle.
*/
