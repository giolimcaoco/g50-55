// activity M.kalalo/discussion

import React from 'react';
import { Link } from 'react-router-dom';


import Banner from '../components/Banner';

export default function Error(){
	const data = {
		title: '404 No Found',
		content: 'The page you are looking for cannot be found',
		destination: '/',
		label: 'Back home'
	}

	return(
		<Banner data={data} />
	)
}


//------------------------------------






// import {Navigate} from 'react-router-dom';
// import {Fragment} from 'react'

// import Banner from '../components/Banner';


// export default function NotFound(){
	
// 	return (
// 		<Fragment>
// 			<Banner name = 'NotFound'/>
// 		</Fragment>
// 		)
// }

// export default function NotFound(){
// 	return(
// 		<Row>
// 			<Col className="p-5">
// 				<h1>Page Not Found</h1>
// 				<p>Go back to the <a href="/#home">homepage</a></p>
// 			</Col>
// 		</Row>
// 	)
// }


// const Error = () => (
//   <div>
//     <h1>Page Not Found!</h1>
//     <Navigate to="/">Go back to homepage</Navigate>
//   </div>
// );

// export default Error;


/*	
	-------------------------Activity S53

	Create a route which will be accessed when a user enters an undefined route and display the Error page.

	Refactor the Banner component to be useable for the Error page and the Home page.

	Push your updates in S50-55 git repo.

	Add another the remote link and push to git with the commit message of Add activity code - S53.

	Add the link in Boodle.
	Sample Output:

*/