// --------------Activity S52

import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom'

// UserContext
import UserContext from '../UserContext'

// sweetalert
import Swal from 'sweetalert2';

export default function Login(){
	
	//allows us to consume the UserContext object and its properties to be used for user validation
	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState ('');
	const [ password, setPassword ] = useState ('');
	const [ isActive, setIsActive ] = useState (false);

	const retrieveUserDetails = (token) => {
		fetch ('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUser ({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if (email !== '' && password !== '' ) {
			setIsActive (true)
		} else {
			setIsActive (false)
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();


		// fetch method will communicate with our backend application providing it with a stringified JSON coming from the body
		/*
			SYNTAX

				fetch('URL', {options})
				.then(res => res.json())
				.then(data => {})

				.then methods are used to create promise chain to catch the data from the fetch method
		*/
		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email, 
				password: password
			})
		})
		.then(res => res.json())
		// good practice output the response in the console to check if the codes are working
		.then(data => {
			console.log(data)
	
			if (typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login successful!",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		})

		/*
			localStorage.getItem is used to get a piece of information from the browsers local storage. 
				devs usually insert information in the local storage for specific purposes; one of commonly used purposed is for loging in. it accepts 1 argument pertaining to the specific key/property inside the local storage to get its value
		*/
		// uses the state hook to set the initial value for user variable coming from localStorage, specifically the email property
		//localStorage.setItem('email', email)

		// setUser ({
		// 	email: localStorage.getItem('email')
		// })



		// clear input fields
		setEmail('');
		setPassword('');
		

		
	}

	


	return(
			(user.id !== null) ?
				<Navigate to='/courses' />
			:
			<Container>
				<h1>Login</h1>
				<Form onSubmit={(e) => loginUser(e)}>
				  <Form.Group className="mb-3" controlId="userEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    		type="email" 
				    		placeholder="Enter email" 
				    		value = {email}
				    		onChange= { e=> setEmail(e.target.value) }
				    		required 
				    		/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="password" required>
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    		type="password" 
				    		placeholder="Password"
				    		value = {password}
				    		onChange= { e=> setPassword(e.target.value) } 
				    		/>
				  </Form.Group>
				
				  { isActive ?
						  <Button variant="success" type="submit" id="submitBtn">
						    Login
						  </Button>
						:
						  <Button variant="success" type="submit" id="submitBtn" disabled>
						    Login
						  </Button>
				  }
				</Form>
			</Container>
		)
	}




