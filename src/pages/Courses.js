import {Fragment, useEffect, useState} from 'react';
import { Container } from 'react-bootstrap';

//import coursesData from '../data/coursesData.js';
import CourseCard from '../components/CourseCard';

/*
	the course in our courseCard component can be sent as a prop
		prop - is a shorthand of "property" since the components are considered as object in React.js

	the curly braces are used for props to signify that we are providing information using JS expressions rather than hard coded values which use double quotes ""
*/
export default function Courses(){
	// console.log (coursesData);

	const [courses, setCourses] = useState ([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course=>{
				return (
					<CourseCard key={course._id} courseProp={course}/>
					)
			}))
		})
	},[])

	/*
		the map method loops through the individual course objects and returns a component for each course
	*/

	// const courses = coursesData.map(course => {
	// 	return(
	// 			<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })

		/*		<h1>Courses</h1>
						<CourseCard courseProp = {coursesData[0]}/>*/
	return(
			<Fragment>
				{courses}
			</Fragment>
	)
}
