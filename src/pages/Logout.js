import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import UserContext from '../UserContext'

export default function Logout() {
	//localStorage.clear();

	// consumes the UserContext object and destructure it to access the unsetUser and setUser
	const {unsetUser, setUser} = useContext(UserContext);
	
	// clear the localStorage
	unsetUser(); 

	/*
		placing the setUser setter function inside of a useEffect is necessary because of the updates in ReactJS: 
			that a state of another component cannot be updated while trying to render a different component
			by adding useEffect, this will render logout page first before triggering the useEffect which changes the state of the user

	*/
	useEffect(()=>{
		// set the user state back to its original state
		setUser({id:null})
	})

	return (
		<Navigate to ='/login'/>
	)
}
